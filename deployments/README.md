# Deployments

> [~](../) > Deployments

This directory contains Helm charts and other Kubernetes deployment manifests for support services to be deployed onto the cluster.

# Bootstrap

The [`./bootstrap` directory](./.bootstrap) contains YAML files that should always be applied. They contain namespace and access management configurations.

# Helm

The [`./helm` directory](./.helm) should be set as the `$HELM_HOME` when applying Helm related resources.

# K9s

The [`./k9s` directory](./.k9s) contains YAML files that should be applied if you'd like to run k9s.

## Services

| Service | Description |
| --- | --- |
| GitLab | Source control management remote service |
| Harbor | Docker image registry |

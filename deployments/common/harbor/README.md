# Harbor

Harbor is a Docker image registry.

# Usage

You'll need Helm installed on the local machine to run this.

Run `make install` to install Harbor for the first time.

The [`./values.yaml`](./values.yaml) is used to configure the Helm deployment.

# Reference Links

1. https://github.com/goharbor/harbor-helm


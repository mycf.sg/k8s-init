locals {
  externaldns_overlay_vars = {
    domain_name = var.domain_name
    zone_id = var.zone_id
  }
}

#
# Argo CD
#

data "archive_file" "argocd" {
  count       = var.install_argocd ? 1 : 0
  type        = "zip"
  source_dir  = "${path.module}/deployments/argocd"
  output_path = "${path.module}/deployments/argocd.zip"
}

resource "local_file" "argocd-overlay" {
  count = var.install_argocd ? 1 : 0
  content = templatefile(
    "${path.module}/deployments/argocd/templates/overlay.yaml",
    { argocd_host = var.argocd_host }
  )
  filename = "${path.module}/deployments/argocd/overlay/overlay.yaml"
}

resource "null_resource" "argocd_kustomize" {
  count = var.install_argocd ? 1 : 0

  triggers = {
    src_hash = data.archive_file.argocd[0].output_sha
  }

  provisioner "local-exec" {
    command = "cd ${path.module}/deployments/argocd && make apply"
  }

  // provisioner "local-exec" {
  //   when    = "destroy"
  //   command = "cd ${path.module}/deployments/argocd && make delete"
  // }

  depends_on = [
    local_file.argocd-overlay
  ]
}

#
# -----------------------------------------------------------------------------
#

#
# ACM
#
module "elb-acm" {
  source  = "terraform-aws-modules/acm/aws"
  version = "~> v2.0"

  domain_name                        = var.domain_name
  subject_alternative_names          = var.subject_alternative_names
  tags                               = var.tags
  validate_certificate               = var.validate_certificate
  validation_allow_overwrite_records = var.validation_allow_overwrite_records
  validation_method                  = var.validation_method
  wait_for_validation                = var.wait_for_validation
  zone_id                            = aws_route53_zone.domain.zone_id

  create_certificate = var.create_acm
}

resource "aws_route53_zone" "domain" {
  name  = var.domain_name
  tags  = var.route53_tags
}

#
# -----------------------------------------------------------------------------
#

#
# externaldns
#

data "archive_file" "externaldns" {
  count       = var.install_externaldns ? 1 : 0
  type        = "zip"
  source_dir  = "${path.module}/deployments/externaldns"
  output_path = "${path.module}/deployments/externaldns.zip"
}

resource "local_file" "externaldns-overlay" {
  count    = var.install_externaldns ? 1 : 0
  content  = templatefile(
    "${path.module}/deployments/externaldns/templates/overlay.yaml",
    local.externaldns_overlay_vars
  )
  filename = "${path.module}/deployments/externaldns/overlay/overlay.yaml"
}

resource "null_resource" "externaldns_kustomize" {
  count     = var.install_externaldns ? 1 : 0

  triggers  = {
    src_hash = data.archive_file.externaldns[0].output_sha
  }

  provisioner "local-exec" {
    command = "cd ${path.module}/deployments/externaldns && make apply"
  }

  provisioner "local-exec" {
    when    = "destroy"
    command = "cd ${path.module}/deployments/externaldns && make delete"
  }

  depends_on = [
    local_file.externaldns-overlay
  ]
}

#
# -----------------------------------------------------------------------------
#

#
# Linkerd
#
resource "null_resource" "linkerd-version" {
  count = var.install_linkerd ? 1 : 0
  provisioner "local-exec" {
    command = "linkerd install --ignore-cluster > .linkerd-temp.yaml"
  }
}

data "archive_file" "linkerd" {
  count       = var.install_linkerd ? 1 : 0
  type        = "zip"
  source_dir  = "${path.module}/deployments/linkerd"
  output_path = "${path.module}/deployments/linkerd.zip"
}

resource "local_file" "linkerd-overlay" {
  count = var.install_linkerd ? 1 : 0
  content = templatefile(
    "${path.module}/deployments/linkerd/templates/overlay.yaml",
    { linkerd_host = var.linkerd_host }
  )
  filename = "${path.module}/deployments/linkerd/overlay/overlay.yaml"
}


resource "null_resource" "linkerd_kustomize" {
  count = var.install_linkerd ? 1 : 0

  triggers = {
    src_hash = data.archive_file.linkerd[0].output_sha
  }

  provisioner "local-exec" {
    command = "cd ${path.module}/deployments/linkerd && make apply"
  }

  provisioner "local-exec" {
    when    = "destroy"
    command = "cd ${path.module}/deployments/linkerd && make delete"
  }

  depends_on = [
    null_resource.linkerd
  ]
}

resource "null_resource" "linkerd" {
  count = var.install_linkerd ? 1 : 0

  triggers = {
    manifest_sha1 = "${sha1(".linkerd-temp.yaml")}"
  }

  provisioner "local-exec" {
    command = "linkerd install -l linkerd --ha | kubectl apply -f"
  }

  provisioner "local-exec" {
    when    = "destroy"
    command = "linkerd install --ignore-cluster -l linkerd --ha | kubectl delete -f -"
  }

  depends_on = [
    null_resource.linkerd-version
  ]
}

#
# -----------------------------------------------------------------------------
#
# k8s-init

Init AMI for provisioning a Kubernetes cluster with helm and istio.

# Usage

## Bootstrap Cluster

Bootstrap the cluster by navigating to [`./deployments`](./deployments) and running `make` there. This should install the required namespaces, service accounts, and roles.

## Install Helm

Helm is used to deploy services when possible. To install it, navigate to [`./tools/helm`](./tools/helm) and follow the instructions there.

# Other Tools

Install the other tools by navigating to the [`./tools` directory](./tools) and selecting the tools you want to install. The standardised Makefile recipe for installing the tool would be `make tldr_install` to retrieve the latest version of it onto the current machine, and `make tldr_install_k8s` to install the software onto the Kubernetes cluster.

# Contents

## Namespaces

Details of this are in [`./deployments/.bootstrap/namespaces.yaml`](./deployments/.bootstrap/namespaces.yaml).

| Namespace | Intention |
| --- | --- |
| `apps` | For logical exposed services |
| `cicd` | For CI/CD agents/runners |
| `common` | For common supporting services required across the cluster |
| `etc` | For miscelleneous services/debugging et cetera |
| `ingress` | For ingress controllers (if any) to live |
| `monitoring` | For monitoring services |

## Access Management

Details of this are in [`./deployments/.bootstrap/access.yaml`](./deployments/.bootstrap/access.yaml)

| Role/Group | Intention |
| --- | --- |
| `gollum` | Reporting - view state of cluster |
| `frodo` | Developers - view everything except secrets, deploy Deployments, Services, ConfigMaps, Jobs, and CronJobs |
| `legolas` | Operators - all permissions except extending access |
| `gandalf` | Administrators - extend access |
| `terraformers` | The EKS terraform module uses `kubectl` to modify the `aws-node` DaemonSets, this role provides minimal permissions so that you can keep your EKS authentication user separate from your Terraform user |

## Tools

| Tool | Description |
| --- | --- |
| Helm 2 | A deployment manager |
| Istio | A full-featured service mesh |
| Kubectl | Used to communicate with Kubernetes cluster |
| LinkerD 2 | A lightweight service mesh |
| Minikube | A single node Kubernetes cluster provisioner |

# Runbook

## Creating a new namespace

> Prerequisites: `kubectl`, working `kubeconfig`, has namespace permissions

1. Add the namespace into [`./deployments/.bootstrap/namespaces.yml`](./deployments/bootstrap/namespaces.yml)
2. Run `kubectl apply ./deployments/.bootstrap/access.yml`
3. Update this README.md

Useful references:
- [Namespace specification](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.15/#namespace-v1-core)

## Creating a new role/group/cluster role

> Prerequisites: `kubectl`, working `kubeconfig`, has role/rolebinding/clusterrole/clusterrolebinding permissions

1. Ask yourself why you are doing this (no, seriously!)
2. Add a Role/ClusterRole resource into [`./deployments/bootstrap/access.yml`](./deployments/bootstrap/access.yml)
3. Add a RoleBinding/ClusterRoleBinding resource into [`./deployments/bootstrap/access.yml`](./deployments/bootstrap/access.yml)
4. Confirm you're applying the resources to the correct server (run `kubectl cluster-info`)
5. Run `kubectl apply ./deployments/bootstrap/access.yml`
6. Update this README.md

Useful references:
- [ClusterRole specification](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.15/#clusterrole-v1-rbac-authorization-k8s-io)
- [ClusterRoleBinding specification](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.15/#clusterrolebinding-v1-rbac-authorization-k8s-io)
- [Role specification](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.15/#role-v1-rbac-authorization-k8s-io)
- [RoleBinding specification](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.15/#rolebinding-v1-rbac-authorization-k8s-io)

## Updating the `k8s-init` AMI

> Prerequisites: `packer`, signed in via `awscli`, following environment variables defined: `AWS_REGION`, `USER`, `VPC_ID`

1. The AMI details are stored in [`./packer.json`](./packer.json), add to it
2. Build it with `make ami`

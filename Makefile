-include ./scripts/Makefile

define HELP_OUTPUT

See the README.md for more information

endef
export HELP_OUTPUT

MINIKUBE_PROFILE=mcf-k8s-init

minikube:
	if ! minikube status --profile $(MINIKUBE_PROFILE); then \
		minikube start \
			--profile $(MINIKUBE_PROFILE) \
			--disk-size 10g \
			--memory 4096 \
			--cpus 4; \
	else \
		printf -- "\n\nthe minikube instance with profile '$(MINIKUBE_PROFILE)' seems to be already started.\n\n"; \
	fi

minikube_kill:
	@minikube stop \
		--profile $(MINIKUBE_PROFILE)
	@minikube delete \
		--profile $(MINIKUBE_PROFILE)

ami:
	@$(MAKE) check_envvar VALUE=${AWS_PROFILE} KEY=AWS_PROFILE
	@$(MAKE) check_envvar VALUE=${AWS_REGION} KEY=AWS_REGION
	@$(MAKE) check_envvar VALUE=${USER} KEY=USER
	@$(MAKE) check_envvar VALUE=${VPC_ID} KEY=VPC_ID
	packer build ./packer.json


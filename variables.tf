variable "create_acm" {
  description = "Creates new certificate for your domain_name"
  type        = bool
  default     = false
}

variable "install_linkerd" {
  description = "Installs linkerd into kubernetes cluster, requires linkerd cli to be installed"
  type        = bool
  default     = false
}

variable "install_argocd" {
  description = "Installs Argo CD into kubernetes cluster"
  type        = bool
  default     = false
}

variable "install_externaldns" {
  description = "Installs external-DNS into kubernetes cluster"
  type        = bool
  default     = false
}

variable "elb_overlay_security_group" {
  description = "security group id to add to elb"
  type        = string
  default     = ""
}

variable "linkerd_host" {
  description = "hostname of linkerd dashboard"
  type        = string
  default     = ""
}

variable "argocd_host" {
  description = "hostname of linkerd dashboard"
  type        = string
  default     = ""
}

# Module Variables ------------------------------------------------------------
# terraform-aws-modules/acm/aws
#

variable "validate_certificate" {
  description = "Whether to validate certificate by creating Route53 record"
  type        = bool
  default     = true
}

variable "validation_allow_overwrite_records" {
  description = "Whether to allow overwrite of Route53 records"
  type        = bool
  default     = true
}

variable "wait_for_validation" {
  description = "Whether to wait for the validation to complete"
  type        = bool
  default     = true
}

variable "domain_name" {
  description = "A domain name for which the certificate should be issued"
  type        = string
  default     = ""
}

variable "subject_alternative_names" {
  description = "A list of domains that should be SANs in the issued certificate"
  type        = list(string)
  default     = []
}

variable "validation_method" {
  description = "Which method to use for validation. DNS or EMAIL are valid, NONE can be used for certificates that were imported into ACM and then into Terraform."
  type        = string
  default     = "DNS"
}

variable "zone_id" {
  description = "The ID of the hosted zone to contain this record."
  type        = string
  default     = ""
}

variable "tags" {
  description = "A mapping of tags to assign to the resource"
  type        = map(string)
  default     = {}
}

variable "route53_tags" {
  description = "A mapping of tags to assign to route53 zone resource"
  type        = map(string)
  default     = {}
}
#
# -----------------------------------------------------------------------------
#
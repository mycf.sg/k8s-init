# Resources

> [~](../../../) > [Tools](../../) > [Helm](../)

These `.yaml` files are not for manual application. They're used by the [`../Makefile`](../Makefile) to generate roles required for the `tiller` user.

# Helm

> [~](../../) > [Tools](../) > Helm

This directory enables:

1. Downloading of the latest Helm
2. Installation of the latest Helm onto local machine
3. Creation of certificates required for Tiller w/ TLS verification
4. Installation of Tiller onto Kubernetes cluster

# Pre-Requisites

1. You should have a running Kubernetes cluster accessible from your current machine
2. You should have a property configured `kubectl` that links to that Kubernetes cluster (run `kubectl cluster-info` to check)

# Get Started

## Install Helm

To install Helm on your local machine assuming no Helm is already installed, run:

```sh
make tldr_install;
```

## Initialise Helm/Tiller

### With default configuration (namespace scoped)

To initialise Helm (and install Tiller onto your cluster), run:

```sh
make tldr_install_k8s NAMESPACE=tiller
```

> The above gives you a highly scoped and secured Tiller instance that does TLS verification and which is scoped to only the namespace specified in `NAMESPACE`. 

### With namespace administrator access (still namespace scoped)

For more liberal Tillers, you may use the following for administration access within a namesapce:

```sh
make tldr_install_k8s_namespaced NAMESPACE=tiller
```

### Cluster level administrator access

Or for a global cluster-wide access:

```sh
make tldr_install_k8s_global;
```

There also exists less secure configurations that you can generate from the undocumented recipes in the Makefile.

To create a new user instead of using the `tiller` user, create a new cert signed with the CA by running:

```sh
make cert_user USER_ID=youruserid
```
